<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$container['db'];

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Exemplo de logs de msgs
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->options('/{routes:.+}', function ($req, $res, $args) {
    return $res;
});

require __DIR__ . '/routes/auth.php';
require __DIR__ . '/routes/users.php';
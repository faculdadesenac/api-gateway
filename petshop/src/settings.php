<?php
return [
    'settings' => [
        'displayErrorDetails' => true, 
        'addContentLengthHeader' => false, r

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        //DB
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'petshop',
            'username'=> 'root',
            'password' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
          ],
          'secretKey' => '837fj28dasdas' //aleatorio
        ],
  ];

